const
	request = require('requestretry'),
	punycode = require('punycode');

module.exports = {
	getTLDsList: () => new Promise((resolve, reject) => request('https://data.iana.org/TLD/tlds-alpha-by-domain.txt', (err, res, body) => {
		if(err)
			reject(err);
		else
			resolve(body
				.toLowerCase()
				.split('\n')
				.filter(l => !!l && !l.startsWith('#'))
				.map(tld => punycode.toUnicode(tld)));
	})),
	findCombination: fullName => new Promise((resolve, reject) => module.exports.getTLDsList().then(domains => {
		resolve(domains.filter(d => !fullName.startsWith(d) && fullName.endsWith(d)).map(d => `${fullName.slice(0, -d.length)}.${d}`));
	}).catch(reject))
};
